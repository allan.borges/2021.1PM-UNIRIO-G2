package data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class StatusTrancaTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void StatusTrancaAposentadaTest() {
		StatusTranca statusTranca = StatusTranca.APOSENTADA;
		
		assertEquals("APOSENTADA", statusTranca.getName());
	}
	
	@Test
	void StatusTrancaEmReparoTest() {
		StatusTranca statusTranca = StatusTranca.EM_REPARO;
		
		assertEquals("EM_REPARO", statusTranca.getName());
	}
	
	@Test
	void StatusTrancaLivreTest() {
		StatusTranca statusTranca = StatusTranca.LIVRE;
		
		assertEquals("LIVRE", statusTranca.getName());
	}
	
	@Test
	void StatusTrancaNovaTest() {
		StatusTranca statusTranca = StatusTranca.NOVA;
		
		assertEquals("NOVA", statusTranca.getName());
	}
	
	@Test
	void StatusTrancaOcupadaTest() {
		StatusTranca statusTranca = StatusTranca.OCUPADA;
		
		assertEquals("OCUPADA", statusTranca.getName());
	}
	
	@Test
	void isValidTest() {
		assertEquals(true, StatusTranca.isValid("OCUPADA"));
	}
	
	@Test
	void isValidFalseTest() {
		assertEquals(false, StatusTranca.isValid("QUALQUER"));
	}

}
