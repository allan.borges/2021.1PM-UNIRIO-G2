package data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import model.Bicicleta;
import model.BicicletaForm;
import model.Totem;
import model.Tranca;
import model.TrancaForm;
import utils.JavalinApp;

class DBTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getBicicleta1Test() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		assertEquals(bicicleta, DB.getBicicleta(bicicleta.getId()));
	}
	
	@Test
	void getBicicletaNullTest() {
		String idBicicleta = "asdf";
		
		assertEquals(null, DB.getBicicleta(idBicicleta));
	}
	
	@Test
	void getBicicleta2Test() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		assertEquals(bicicleta, DB.getBicicleta(bicicleta.getNumero()));
	}
	
	@Test
	void getBicicletaFromTrancaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		String bicicleta = tranca.getBicicleta();
		
		assertEquals(bicicleta, DB.getBicicletaFromTranca(tranca.getId()).getId());
	}
	
	@Test
	void getBicicletaFromTrancaNullTest() {
		String idTranca = "asdf";
		
		assertEquals(null, DB.getBicicletaFromTranca(idTranca));
	}
	
	@Test
	void getTotemTest() {
		Totem totem = DB.getTotens().get(0);
		
		assertEquals(totem, DB.getTotem(totem.getId()));
	}
	
	@Test
	void getTrancaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		
		assertEquals(tranca, DB.getTranca(tranca.getId()));
	}
	
	@Test
	void addBicicletaTest() {
		Bicicleta bicicleta = new Bicicleta("Qualquer marca", "Qualquer modelo", "2020", 999, "ativo");
		
		DB.addBicicleta(bicicleta);
		
		assertEquals(bicicleta, DB.getBicicleta(999));
	}
	
	@Test
	void addTotemTest() {
		Totem totem = new Totem("Botafogo");
		String id = totem.getId();
		
		DB.addTotem(totem);
		
		assertEquals(id, DB.getTotem(id).getId());
	}
	
	@Test
	void addTrancaTest() {
		Tranca tranca = new Tranca(999, "Botafogo", "2020", "Qualquer modelo", "ativo");
		String id = tranca.getId();
		
		DB.addTranca(tranca);
		
		assertEquals(tranca, DB.getTranca(id));
	}
	
	@Test
	void deleteBicicletaTest() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		String id = bicicleta.getId();
		
		DB.deleteBicicleta(id);
		
		assertEquals(null, DB.getBicicleta(id));
	}
	
	@Test
	void deleteTotemTest() {
		Totem totem = DB.getTotens().get(0);
		String id = totem.getId();
		
		DB.deleteTotem(id);
		
		assertEquals(null, DB.getTotem(id));
	}
	
	@Test
	void deleteTrancaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		String id = tranca.getId();
		
		DB.deleteTranca(id);
		
		assertEquals(null, DB.getTranca(id));
	}
	
	@Test
	void integrarBicicletaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		String idTranca = tranca.getId();
		String idBicicleta = bicicleta.getId();
		
		DB.integrarBicicleta(idTranca, idBicicleta);
		
		assertEquals(idBicicleta, DB.getTranca(idTranca).getBicicleta());
	}
	
	@Test
	void integrarBicicletaNullBicicletaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		
		String idTranca = tranca.getId();
		String idBicicleta = "asdf";
		
		DB.integrarBicicleta(idTranca, idBicicleta);
		
		assertEquals(null, DB.getBicicleta(idBicicleta));
	}
	
	@Test
	void integrarBicicletaNullTrancaTest() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		String idTranca = "asdf";
		String idBicicleta = bicicleta.getId();
		
		DB.integrarBicicleta(idTranca, idBicicleta);
		
		assertEquals(null, DB.getTranca(idTranca));
	}
	
	@Test
	void integrarTrancaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		Totem totem = DB.getTotens().get(0);
		
		String idTranca = tranca.getId();
		String idTotem = totem.getId();
		
		DB.integrarTranca(idTranca, idTotem);
		
		assertEquals(true, DB.getTrancasFromTotem(idTotem).contains(tranca));
	}
	
	@Test
	void getTrancasFromTotemNullTest() {
		String idTotem = "asdf";
		
		assertEquals(0, DB.getTrancasFromTotem(idTotem).size());
	}
	
	@Test
	void getBicicletasFromTotemTest() {
		Totem totem = DB.getTotens().get(0);
		
		String idTotem = totem.getId();
		
		assertEquals(0, DB.getBicicletasFromTotem(idTotem).size());
	}
	
	@Test
	void getBicicletasFromTotemNullTest() {
		String idTotem = "asdf";
		
		assertEquals(0, DB.getBicicletasFromTotem(idTotem).size());
	}
	
	@Test
	void integrarTrancaNullTrancaTest() {
		Totem totem = DB.getTotens().get(0);
		
		String idTranca = "asdf";
		String idTotem = totem.getId();
		
		DB.integrarTranca(idTranca, idTotem);
		
		assertEquals(null, DB.getTranca(idTranca));
	}
	
	@Test
	void integrarTrancaNullTotemTest() {
		Tranca tranca = DB.getTrancas().get(0);
		
		String idTranca = tranca.getId();
		String idTotem = "asdf";
		
		DB.integrarTranca(idTranca, idTotem);
		
		assertEquals(null, DB.getTotem(idTotem));
	}
	
	@Test
	void retirarBicicletaTest() {		
		Tranca tranca = DB.getTrancas().get(0);
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		String idTranca = tranca.getId();
		String idBicicleta = bicicleta.getId();
		
		DB.retirarBicicleta(idTranca, idBicicleta);
		
		assertEquals("", DB.getTranca(idTranca).getBicicleta());
	}
	
	@Test
	void retirarBicicletaNullBicicletaTest() {		
		Tranca tranca = DB.getTrancas().get(0);
		
		String idTranca = tranca.getId();
		String idBicicleta = "asdf";
		
		DB.retirarBicicleta(idTranca, idBicicleta);
		
		assertEquals(null, DB.getBicicleta(idBicicleta));
	}
	
	@Test
	void retirarBicicletaNullTrancaTest() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		String idTranca = "asdf";
		String idBicicleta = bicicleta.getId();
		
		DB.retirarBicicleta(idTranca, idBicicleta);
		
		assertEquals(null, DB.getTranca(idTranca));
	}
	
	@Test
	void retirarTrancaTest() {		
		Tranca tranca = DB.getTrancas().get(0);
		
		String idTranca = tranca.getId();
			
		DB.retirarTranca(idTranca);
		
		assertEquals("EM_REPARO", DB.getTranca(idTranca).getStatus());
	}
	
	@Test
	void retirarTrancaNullTrancaTest() {
		String idTranca = "asdf";
			
		DB.retirarTranca(idTranca);
		
		assertEquals(null, DB.getTranca(idTranca));
	}
	
	@Test
	void updateBicicletaTest() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		String idBicicleta = bicicleta.getId();
		
		DB.updateBicicleta(idBicicleta, new BicicletaForm());
		
		assertEquals(null, DB.getBicicleta(idBicicleta).getAno());
	}
	
	@Test
	void updateStatusBicicletaTest() {
		Bicicleta bicicleta = DB.getBicicletas().get(0);
		
		String idBicicleta = bicicleta.getId();
		
		DB.updateStatusBicicleta(idBicicleta, "inativo");
		
		assertEquals("inativo", DB.getBicicleta(idBicicleta).getStatus());
	}
	
	@Test
	void updateStatusTrancaTest() {
		Tranca tranca = DB.getTrancas().get(0);
		
		String idTranca = tranca.getId();
		
		DB.updateStatusTranca(idTranca, "inativo");
		
		assertEquals("inativo", DB.getTranca(idTranca).getStatus());
	}
	
	@Test
	void updateTotemTest() {		
		Totem totem = DB.getTotens().get(0);
		
		String idTotem = totem.getId();
		
		DB.updateTotem(idTotem, "Urca");
		
		assertEquals("Urca", DB.getTotem(idTotem).getLocalizacao());
	}

	@Test
	void updateTrancaTest() {		
		Tranca tranca = DB.getTrancas().get(0);
		
		String idTranca = tranca.getId();
		
		DB.updateTranca(idTranca, new TrancaForm());;
		
		assertEquals(0, DB.getTranca(idTranca).getNumero());
	}
	
	@Test
	void DBInstaciaTest() {
		DB db = new DB();
		
		assertEquals(DB.class, db.getClass());
	}

}
