package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class RetirarTrancaFormTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getsRetirarTrancaFormTest() {
		RetirarTrancaForm retirarTrancaForm = new RetirarTrancaForm();
		
		assertEquals(null, retirarTrancaForm.getIdTotem());
		assertEquals(null, retirarTrancaForm.getIdTranca());
	}

}
