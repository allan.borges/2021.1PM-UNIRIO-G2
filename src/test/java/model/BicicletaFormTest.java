package model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import utils.JavalinApp;

class BicicletaFormTest {

	private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

	@BeforeAll
	static void init() {
		app.start(7000);
		
	}

	@AfterAll
	static void afterAll() {
		app.stop();
	}
	
	@Test
	void getsBicicletaFormTest() {
		BicicletaForm bicicletaForm = new BicicletaForm();
		
		assertEquals(null, bicicletaForm.getMarca());
		assertEquals(null, bicicletaForm.getModelo());
		assertEquals(null, bicicletaForm.getAno());
		assertEquals(0, bicicletaForm.getNumero());
		assertEquals(null, bicicletaForm.getStatus());
	}

}
