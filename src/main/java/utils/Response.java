package utils;

import java.util.HashMap;
import java.util.Map;

import io.javalin.http.Context;

public class Response {
	Response() {
		// util
	}

	private static Map<String, Object> getErrorResponse(String id, int codigo, String mensagem) {
		Map<String, Object> response = new HashMap<>();
		response.put("id", id);
		response.put("codigo", codigo);
		response.put("mensagem", mensagem);
		return response;
	}

	public static void setErrorResponse(String id, int codigo, String mensagem, Context ctx) {
		Map<String, Object> response = Response.getErrorResponse(id, codigo, mensagem);
		ctx.json(response);
		ctx.status(codigo);
	}
}
