package utils;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.put;

import controller.BicicletaController;
import controller.TotemController;
import controller.TrancaController;
import io.javalin.Javalin;

public class JavalinApp {
	private Javalin app = Javalin.create(config -> config.defaultContentType = "application/json").routes(() -> {
		path("/bicicleta", () -> {
			get(BicicletaController::getBicicletas);
			post(BicicletaController::createBicicleta);
			path("/:idBicicleta", () -> {
				get(BicicletaController::getBicicleta);
				put(BicicletaController::updateBicicleta);
				delete(BicicletaController::deleteBicicleta);
				path("/status/:acao", () -> post(BicicletaController::updateStatus));
			});
			path("/integrarNaRede", () -> post(BicicletaController::integrarBicicletaNaRede));
			path("/retirarDaRede", () -> post(BicicletaController::retirarBicicletaDaRede));
		});
		path("/totem", () -> {
			get(TotemController::getTotens);
			post(TotemController::createTotem);
			path("/:idTotem", () -> {
				put(TotemController::updateTotem);
				delete(TotemController::deleteTotem);
				path("/trancas", () -> get(TotemController::getTrancas));
				path("/bicicletas", () -> get(TotemController::getBicicletas));
			});
		});
		path("/tranca", () -> {
			get(TrancaController::getTrancas);
			post(TrancaController::createTranca);
			path("/:idTranca", () -> {
				get(TrancaController::getTranca);
				put(TrancaController::updateTranca);
				delete(TrancaController::deleteTranca);
				path("/bicicleta", () -> get(TrancaController::getBicicleta));
				path("/status/:acao", () -> post(TrancaController::updateStatus));
			});
			path("/integrarNaRede", () -> post(TrancaController::integrarTrancaNaRede));
			path("/retirarDaRede", () -> post(TrancaController::retirarTrancaDaRede));
		});
	});

	public void start(int port) {
		this.app.start(port);
	}

	public void stop() {
		this.app.stop();
	}
}
