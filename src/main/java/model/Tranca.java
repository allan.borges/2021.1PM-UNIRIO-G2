package model;

import java.util.UUID;

public class Tranca {
	private String id, bicicleta;
	private int numero;
	private String localizacao, anoDeFabricacao, modelo, status;

	public Tranca(int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
		this.id = UUID.randomUUID().toString();
		this.bicicleta = "";
		this.numero = numero;
		this.localizacao = localizacao;
		this.anoDeFabricacao = anoDeFabricacao;
		this.modelo = modelo;
		this.status = status;
	}

	public Tranca(String bicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo,
			String status) {
		this.id = UUID.randomUUID().toString();
		this.bicicleta = bicicleta;
		this.numero = numero;
		this.localizacao = localizacao;
		this.anoDeFabricacao = anoDeFabricacao;
		this.modelo = modelo;
		this.status = status;
	}

	public String getBicicleta() {
		return bicicleta;
	}

	public void setBicicleta(String bicicleta) {
		this.bicicleta = bicicleta;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public String getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(String anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

}
