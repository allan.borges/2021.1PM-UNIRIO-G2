package model;

import java.util.UUID;

public class Bicicleta {
	private String id, marca, modelo, ano;
	private int numero;
	private String status;

	public Bicicleta(String marca, String modelo, String ano, int numero, String status) {
		this.id = UUID.randomUUID().toString();
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.numero = numero;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
}
