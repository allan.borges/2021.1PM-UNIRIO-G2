package model;

import java.util.UUID;

public class Totem {
	private String id, localizacao;

	public Totem(String localizacao) {
		this.id = UUID.randomUUID().toString();
		this.localizacao = localizacao;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getId() {
		return id;
	}
}
