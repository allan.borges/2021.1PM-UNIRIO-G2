package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import data.DB;
import data.StatusTranca;
import io.javalin.http.Context;
import model.IntegrarTrancaForm;
import model.RetirarTrancaForm;
import model.Totem;
import model.Tranca;
import model.TrancaForm;
import utils.Response;

public class TrancaController {
	private static String pathIdTranca = "idTranca";
	private static String trancaNaoEncontrada = "Tranca não encontrada.";
	private static String dadosInvalidos = "Dados Inválidos: ";
	private static String existeCampoEmBranco = "existe campo em branco.";

	TrancaController() {
		// Controller
	}

	public static void getTrancas(Context ctx) {
		ctx.json(DB.getTrancas().toArray());
		ctx.status(200);
	}

	public static void getTranca(Context ctx) {
		String idTranca = ctx.pathParam(pathIdTranca);

		try {
			ctx.json(DB.getTranca(idTranca));
			ctx.status(200);
		} catch (Exception e) {
			Response.setErrorResponse(idTranca, 404, trancaNaoEncontrada, ctx);
		}
	}

	public static void getBicicleta(Context ctx) {
		String idTranca = ctx.pathParam(pathIdTranca);

		try {
			ctx.json(DB.getTranca(idTranca));
		} catch (Exception e) {
			Response.setErrorResponse(idTranca, 422, "Id da tranca inválido.", ctx);
			return;
		}

		try {
			ctx.json(DB.getBicicletaFromTranca(idTranca));
			ctx.status(200);
		} catch (Exception e) {
			Response.setErrorResponse(idTranca, 404, "Bicicleta não encontrada.", ctx);
		}
	}

	public static void integrarTrancaNaRede(Context ctx) throws IOException {
		IntegrarTrancaForm integrarTrancaForm;

		try {
			integrarTrancaForm = ctx.bodyAsClass(IntegrarTrancaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, dadosInvalidos + e.getMessage(), ctx);
			return;
		}

		String idTotem = integrarTrancaForm.getIdTotem();
		String idTranca = integrarTrancaForm.getIdTranca();
		Totem totem = DB.getTotem(idTotem);
		Tranca tranca = DB.getTranca(idTranca);

		if (idTotem != null && idTranca != null && !idTotem.equals("") && !idTranca.equals("")) {
			if (totem != null && tranca != null) {
				if (tranca.getStatus().equals(StatusTranca.EM_REPARO.getName())
						|| tranca.getStatus().equals(StatusTranca.NOVA.getName())) {
					DB.integrarTranca(idTranca, idTotem);
					ctx.status(200);
				} else {
					Response.setErrorResponse(idTranca, 422, "Dados Inválidos: status da tranca inválido.", ctx);
				}
			} else {
				Response.setErrorResponse("", 422, "Dados Inválidos: totem ou tranca não encontrado.", ctx);
			}
		} else {
			Response.setErrorResponse("", 422, dadosInvalidos + existeCampoEmBranco, ctx);
		}
	}

	public static void retirarTrancaDaRede(Context ctx) {
		RetirarTrancaForm retirarTrancaForm;

		try {
			retirarTrancaForm = ctx.bodyAsClass(RetirarTrancaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, dadosInvalidos + e.getMessage(), ctx);
			return;
		}

		String idTotem = retirarTrancaForm.getIdTotem();
		String idTranca = retirarTrancaForm.getIdTranca();
		Totem totem = DB.getTotem(idTotem);
		Tranca tranca = DB.getTranca(idTranca);

		if (idTotem != null && idTranca != null && !idTotem.equals("") && !idTranca.equals("")) {
			if (totem != null && tranca != null) {
				if (tranca.getStatus().equals(StatusTranca.LIVRE.getName())) {
					DB.retirarTranca(idTranca);
					ctx.status(200);
				} else {
					Response.setErrorResponse(idTranca, 422, "Dados Inválidos: status da tranca inválido.", ctx);
				}
			} else {
				Response.setErrorResponse("", 422, "Dados Inválidos: totem ou tranca não encontrado.", ctx);
			}
		} else {
			Response.setErrorResponse("", 422, dadosInvalidos + existeCampoEmBranco, ctx);
		}
	}

	public static void createTranca(Context ctx) {
		Map<String, Object> trancaValues = getTrancaValues(ctx);

		int numero = (int) trancaValues.get("numero");
		String localizacao = trancaValues.get("localizacao").toString();
		String anoDeFabricacao = trancaValues.get("anoDeFabricacao").toString();
		String modelo = trancaValues.get("modelo").toString();
		String status = trancaValues.get("status").toString();

		if (numero != 0 && localizacao != null && anoDeFabricacao != null && modelo != null && status != null
				&& !localizacao.equals("") && !anoDeFabricacao.equals("") && !modelo.equals("") && !status.equals("")) {
			Tranca tranca = new Tranca(numero, localizacao, anoDeFabricacao, modelo, status);

			DB.addTranca(tranca);

			try {
				ctx.json(DB.getTranca(tranca.getId()));
				ctx.status(200);
				return;
			} catch (Exception e) {
				Response.setErrorResponse("", 422, "Tranca não criada: " + e.getMessage(), ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, dadosInvalidos + existeCampoEmBranco, ctx);
			return;
		}
	}

	public static void updateStatus(Context ctx) {
		String idTranca = ctx.pathParam(pathIdTranca);
		String acao = ctx.pathParam("acao");

		try {
			ctx.json(DB.getTranca(idTranca));
		} catch (Exception e) {
			Response.setErrorResponse(idTranca, 404, trancaNaoEncontrada, ctx);
			return;
		}

		if (acao.equals("DESTRANCAR") || acao.equals("TRANCAR")) {
			if (acao.equals("DESTRANCAR")) {
				DB.updateStatusTranca(idTranca, StatusTranca.LIVRE.getName());
			} else {
				DB.updateStatusTranca(idTranca, StatusTranca.OCUPADA.getName());
			}

			ctx.json(DB.getTranca(idTranca));
			ctx.status(200);
		} else {
			Response.setErrorResponse(idTranca, 422, "Dados Inválidos: ação não válida.", ctx);
		}
	}

	public static void updateTranca(Context ctx) {
		String idTranca = ctx.pathParam(pathIdTranca);

		if (DB.getTranca(idTranca) == null) {
			Response.setErrorResponse(idTranca, 404, trancaNaoEncontrada, ctx);
			return;
		}

		TrancaForm trancaForm;

		try {
			trancaForm = ctx.bodyAsClass(TrancaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, dadosInvalidos + e.getMessage(), ctx);
			return;
		}

		int numero = trancaForm.getNumero();
		String localizacao = trancaForm.getLocalizacao();
		String anoDeFabricacao = trancaForm.getAnoDeFabricacao();
		String modelo = trancaForm.getModelo();
		String status = trancaForm.getStatus();

		if (numero != 0 && localizacao != null && anoDeFabricacao != null && modelo != null && status != null
				&& !localizacao.equals("") && !anoDeFabricacao.equals("") && !modelo.equals("") && !status.equals("")) {
			DB.updateTranca(idTranca, trancaForm);

			try {
				ctx.json(DB.getTranca(idTranca));
				ctx.status(200);
				return;
			} catch (Exception e) {
				Response.setErrorResponse("", 422, "Tranca não editada: " + e.getMessage(), ctx);
				return;
			}
		} else {
			Response.setErrorResponse("", 422, dadosInvalidos + existeCampoEmBranco, ctx);
			return;
		}
	}

	public static void deleteTranca(Context ctx) {
		String idTranca = ctx.pathParam(pathIdTranca);

		if (DB.getTranca(idTranca) == null) {
			Response.setErrorResponse(idTranca, 404, trancaNaoEncontrada, ctx);
			return;
		}

		DB.deleteTranca(idTranca);

		ctx.status(200);
		return;
	}

	private static Map<String, Object> getTrancaValues(Context ctx) {
		TrancaForm trancaForm;

		try {
			trancaForm = ctx.bodyAsClass(TrancaForm.class);
		} catch (Exception e) {
			Response.setErrorResponse("", 422, dadosInvalidos + e.getMessage(), ctx);
			return new HashMap<>();
		}

		int numero = trancaForm.getNumero();
		String localizacao = trancaForm.getLocalizacao();
		String anoDeFabricacao = trancaForm.getAnoDeFabricacao();
		String modelo = trancaForm.getModelo();
		String status = trancaForm.getStatus();

		Map<String, Object> data = new HashMap<>();
		data.put("numero", numero);
		data.put("localizacao", localizacao);
		data.put("anoDeFabricacao", anoDeFabricacao);
		data.put("modelo", modelo);
		data.put("status", status);

		return data;
	}
}